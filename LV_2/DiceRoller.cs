﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_2
{
    class DiceRoller
    {
        private List<Die> dice;
        private List<int> resultforEachRoll;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultforEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RollAllDice()
        {
            this.resultforEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultforEachRoll.Add(die.Roll());
            }
        }

        public List<int> GetRollingResults()
        {
            //return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
             // this.resultforEachRoll);
            return resultforEachRoll;
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }
    }
}
