﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_2
{
    //1.ZAD
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        DiceRoller dice_Roller = new DiceRoller();
    //        Die[] Die = new Die[20];

    //        for(int i = 0; i < 20; i++)
    //        {
    //            Random random_Value = new Random();
    //            int random_Sides = random_Value.Next(4, 7);
    //            //broj stranica izmedu 4 i 6, jer ne vidim kako bi se napravila "kockica" sa 3 strane
    //            //4 i 5 stranica bi onda bila trostrana ili cetverostrana piramida, 6 normalna kockica
    //            Die[i] = new Die(random_Sides);
    //            dice_Roller.InsertDie(Die[i]);
    //        }
    //        dice_Roller.RollAllDice();
    //        List<int> Results = dice_Roller.GetRollingResults();
    //        foreach(int value in Results)
    //        {
    //            Console.WriteLine(value);
    //        }
    //        Console.ReadLine();
    //    }
    //}
    //2.ZAD
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        DiceRoller dice_Roller = new DiceRoller();
    //        Die[] Die = new Die[20];

    //        for (int i = 0; i < 20; i++)
    //        {
    //            Random random_Value = new Random();
    //            int random_Sides = random_Value.Next(4, 7);
                  //broj stranica izmedu 4 i 6, jer ne vidim kako bi se napravila "kockica" sa 3 strane
                  //4 i 5 stranica bi onda bila trostrana ili cetverostrana piramida, 6 normalna kockica
    //            Die[i] = new Die(random_Sides, random_Value.Next(1, random_Sides + 1));
    //            dice_Roller.InsertDie(Die[i]);
    //        }
      //        dice_Roller.RollAllDice();
      //        List<int> Results = dice_Roller.GetRollingResults();
      //        foreach (int value in Results)
      //        {
      //           Console.WriteLine(value);
      //        }
      //        Console.ReadLine();
      //    }
      //}
    //3.ZAD
    class Program
{
    static void Main(string[] args)
    {
        DiceRoller dice_Roller = new DiceRoller();
        Die[] Die = new Die[20];

        for (int i = 0; i < 20; i++)
        {
            Random random_Sides = new Random();
            Die[i] = new Die(random_Sides.Next(1, 7));
            dice_Roller.InsertDie(Die[i]);
        }

        dice_Roller.RollAllDice();
        List<int> Results = dice_Roller.GetRollingResults();
        foreach (int value in Results)
        {
            Console.WriteLine(value);
        }

        Console.ReadLine();
    }
}

}
