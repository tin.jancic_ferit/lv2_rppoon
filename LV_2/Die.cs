﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_2
{
    //1.ZAD
    //class Die
    //{
    //    private int numberOfSides;

    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //    }

    //    public int Roll()
    //    {
    //        Random randomGenerator = new Random();
    //        int rolledNumber = randomGenerator.Next(1, this.numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}
    //2.ZAD
    //class Die
    //{
    //    private int numberOfSides;
    //    private int rolledValues;

    //    public Die(int numberOfSides, int rolledValues)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.rolledValues = rolledValues;
    //    }

    //    public int Roll()
    //    {
    //        return rolledValues;
    //    }
    //}
    //3.ZAD
    class Die
    {
        private int numberOfSides;
        private RandomGenerator rolledValues;

        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.rolledValues = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            return rolledValues.NextInt(1, 7);
        }
        public int NumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }

}
